#-------------------------------------------------
#
# Project created by QtCreator 2012-03-29T16:12:22
#
#-------------------------------------------------

QT       += core gui network

TARGET = spok
TEMPLATE = app
LIBS += -lssh

SOURCES += main.cpp\
        mainwindow.cpp \
    sshconnection.cpp

HEADERS  += mainwindow.h \
    sshconnection.h

FORMS    += mainwindow.ui \
    settings.ui \
    bulkadd.ui

RESOURCES += \
    spok.qrc
