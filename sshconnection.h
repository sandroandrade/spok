#ifndef SSHCONNECTION_H
#define SSHCONNECTION_H

#include <QtCore/QObject>

#include <libssh/libssh.h>

class SshConnection : public QObject
{
    Q_OBJECT
public:
    explicit SshConnection(QString host, QObject *parent = 0);
    ~SshConnection();

    enum State {DISCONNECTED = 0, CONNECTING, CONNECTED, ERROR};
signals:
    
public slots:
    void setHost(QString host);
    void connect(QString username, QString password);
    void connect(QString privateKeyFile);
    void disconnect();

private:
    ssh_session _ssh_session;
    QString _host;

    State _state;
};

#endif // SSHCONNECTION_H
