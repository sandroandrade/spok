#include "sshconnection.h"

#include <QtCore/QDebug>

SshConnection::SshConnection(QString host, QObject *parent) :
    QObject(parent), _state(DISCONNECTED)
{
    _ssh_session = ssh_new();
    if (_ssh_session == NULL)
    {
        _state = ERROR;
        return;
    }
    setHost(host);
}

SshConnection::~SshConnection()
{
    if (_state == CONNECTED)
        ssh_disconnect(_ssh_session);
    if (_state != ERROR)
        ssh_free(_ssh_session);
}

void SshConnection::setHost(QString host)
{
    _host = host;
    ssh_options_set(_ssh_session, SSH_OPTIONS_HOST, host.toLatin1());
}

void SshConnection::connect(QString username, QString password)
{
    _state = CONNECTING;
    int rc = ssh_connect(_ssh_session);
    if (rc != SSH_OK)
    {
        qDebug() << QString("Error connecting to localhost %1. Error: %2.").arg(_host).arg(ssh_get_error(_ssh_session));
        _state = ERROR;
        return;
    }

    rc = ssh_userauth_list(_ssh_session, 0);
    if (rc & SSH_AUTH_METHOD_NONE)
        qDebug() << "None";
    if (rc & SSH_AUTH_METHOD_PUBLICKEY)
        qDebug() << "PubKey";
    if (rc & SSH_AUTH_METHOD_INTERACTIVE)
        qDebug() << "Interactive";
    if (rc & SSH_AUTH_METHOD_PASSWORD)
        qDebug() << "Password";

    rc = ssh_userauth_password(_ssh_session, username.toLatin1(), password.toLatin1());
    if (rc != SSH_AUTH_SUCCESS)
    {
        qDebug() << QString("Error authenticating with password: %1.").arg(ssh_get_error(_ssh_session));
        ssh_disconnect(_ssh_session);
        _state = ERROR;
    }
    if (rc == SSH_AUTH_ERROR) qDebug() << "SSH_AUTH_ERROR";
    if (rc == SSH_AUTH_DENIED) qDebug() << "SSH_AUTH_DENIED";
    if (rc == SSH_AUTH_PARTIAL) qDebug() << "SSH_AUTH_PARTIAL";
    if (rc == SSH_AUTH_AGAIN) qDebug() << "SSH_AUTH_AGAIN";
    _state = CONNECTED;
}

void SshConnection::connect(QString privateKeyFile)
{
    _state = CONNECTING;
    int rc = ssh_connect(_ssh_session);
    if (rc != SSH_OK)
    {
        qDebug() << QString("Error connecting to localhost %1. Error: %2.").arg(_host).arg(ssh_get_error(_ssh_session));
        _state = ERROR;
        return;
    }

    rc = ssh_userauth_list(_ssh_session, 0);
    if (rc & SSH_AUTH_METHOD_NONE)
        qDebug() << "None";
    if (rc & SSH_AUTH_METHOD_PUBLICKEY)
        qDebug() << "PubKey";
    if (rc & SSH_AUTH_METHOD_INTERACTIVE)
        qDebug() << "Interactive";
    if (rc & SSH_AUTH_METHOD_PASSWORD)
        qDebug() << "Password";

    rc = ssh_userauth_privatekey_file(_ssh_session, "sandroandrade", privateKeyFile.toLatin1(), "");
    if (rc != SSH_AUTH_SUCCESS)
    {
        qDebug() <<  QString("Authentication failed: %1.").arg(ssh_get_error(_ssh_session));
        _state = ERROR;
        return;
    }
    if (rc == SSH_AUTH_ERROR) qDebug() << "SSH_AUTH_ERROR";
    if (rc == SSH_AUTH_DENIED) qDebug() << "SSH_AUTH_DENIED";
    if (rc == SSH_AUTH_PARTIAL) qDebug() << "SSH_AUTH_PARTIAL";
    if (rc == SSH_AUTH_AGAIN) qDebug() << "SSH_AUTH_AGAIN";
    if (rc == SSH_AUTH_SUCCESS) qDebug() << "SSH_AUTH_SUCCESS";
    _state = CONNECTED;
}

void SshConnection::disconnect()
{
    if (_state == CONNECTED)
        ssh_disconnect(_ssh_session);
}
