#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtCore/QList>

#include <QtGui/QMainWindow>

#include <QtNetwork/QAbstractSocket>

class QDialog;
class QTcpSocket;

namespace Ui
{
    class MainWindow;
    class Settings;
    class BulkAdd;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private:
    void readSettings();
    void writeSettings();

private Q_SLOTS:
    void on_actionAddHub_triggered();
    void on_actionBulkAddHub_triggered();
    void on_actionRemoveHub_triggered();
    void on_actionAddHost_triggered();
    void on_actionBulkAddHost_triggered();
    void on_actionRemoveHost_triggered();
    void on_actionScanNetwork_triggered();
    void on_actionConnect_triggered();
    void on_actionDisconnect_triggered();
    void on_actionSpokSettings_triggered();
    void on_psbCancelNetworkScan_clicked();

    void socketStateChanged(QAbstractSocket::SocketState state);

    void rdbKeyPairClicked();
    void rdbUsernamePasswordClicked();

    void tcpSocketTimeout();

private:
    void addHub(const QString &hubName);
    void addHost(const QString &hostName);
    void resetBulkAddDialog();
    void confirmBulkAdd();

    Ui::MainWindow *ui;

    Ui::Settings *settings;
    QDialog *settingsDialog;

    Ui::BulkAdd *bulkAdd;
    QDialog *bulkAddDialog;

    QTcpSocket *_tcpSocket;
    QList<QString> _detectedHosts;
    int _octect[4];
    bool _octectFixed[4];
    int _iteratingOctect;
    int _lastIteratingOctect;
    bool _scanCanceled;
};

#endif // MAINWINDOW_H
