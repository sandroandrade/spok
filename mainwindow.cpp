#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "ui_settings.h"
#include "ui_bulkadd.h"

#include <math.h>

#include <QtCore/QDir>
#include <QtCore/QTimer>
#include <QtCore/QDebug>
#include <QtCore/QSettings>

#include <QtGui/QLineEdit>
#include <QtGui/QMessageBox>
#include <QtGui/QInputDialog>

#include <QtNetwork/QTcpSocket>
#include <QtNetwork/QHostAddress>

#include <sshconnection.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    settings(new Ui::Settings),
    settingsDialog(new QDialog(this)),
    bulkAdd(new Ui::BulkAdd),
    bulkAddDialog(new QDialog(this)),
    _tcpSocket (new QTcpSocket(this))
{
    ui->setupUi(this);

    ui->dckNetworkScan->setVisible(false);

    ui->trwHostArrays->topLevelItem(0)->setSelected(true);
    ui->trwHostArrays->topLevelItem(0)->setData(0, Qt::UserRole, "hub");

    settings->setupUi(settingsDialog);
    readSettings();
    connect(settings->rdbKeyPair, SIGNAL(clicked()), SLOT(rdbKeyPairClicked()));
    connect(settings->rdbUsernamePassword, SIGNAL(clicked()), SLOT(rdbUsernamePasswordClicked()));

    bulkAdd->setupUi(bulkAddDialog);

    SshConnection ssh("localhost");
    ssh.connect("/home/sandroandrade/.ssh/id_rsa");

    connect(_tcpSocket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), SLOT(socketStateChanged(QAbstractSocket::SocketState)));
}

MainWindow::~MainWindow()
{
    delete ui;
    delete settings;
    delete bulkAdd;
}

void MainWindow::readSettings()
{
    QSettings settingGroup("LiveBlue", "Spok");
    settingGroup.beginGroup("auth");
    settings->rdbKeyPair->setChecked(settingGroup.value("authType").toString() == "KeyPair");
    settings->rdbKeyPair->setChecked(settingGroup.value("authType").toString() == "UsernamePassword");
    settings->lneKeyFilesPrefix->setText(settingGroup.value("keyFilesPrefix", "~/.ssh/id_rsa").toString());
    settings->lnePassphrase->setText(settingGroup.value("passphrase").toString());
    settings->lneUsername->setText(settingGroup.value("username").toString());
    settings->lnePassword->setText(settingGroup.value("password").toString());
    settings->spbNetworkScanTimeout->setValue(settingGroup.value("networkScanTimeout", 2000).toInt());
    settingGroup.endGroup();
}

void MainWindow::writeSettings()
{
    QSettings settingGroup("LiveBlue", "Spok");
    settingGroup.beginGroup("auth");
    settingGroup.setValue("authType", settings->rdbKeyPair->isEnabled() ? "KeyPair":"UsernamePassword");
    settingGroup.setValue("keyFilesPrefix", settings->lneKeyFilesPrefix->text());
    settingGroup.setValue("passphrase", settings->lnePassphrase->text());
    settingGroup.setValue("username", settings->lneUsername->text());
    settingGroup.setValue("password", settings->lnePassword->text());
    settingGroup.setValue("networkScanTimeout", settings->spbNetworkScanTimeout->value());
    settingGroup.endGroup();
}

void MainWindow::on_actionAddHub_triggered()
{
    if (ui->trwHostArrays->selectedItems()[0]->data(0, Qt::UserRole).toString() != "hub")
    {
        QMessageBox::warning(this, tr("Add Hub"), tr("You should select a hub as the parent of the new hub !"));
        return;
    }
    bool ok;
    QString hubName = QInputDialog::getText(this, tr("Add Hub"), tr("Hub name:"), QLineEdit::Normal, "", &ok);
    if (ok && !hubName.isEmpty())
        addHub(hubName);
}

void MainWindow::on_actionBulkAddHub_triggered()
{
    if (ui->trwHostArrays->selectedItems()[0]->data(0, Qt::UserRole).toString() != "hub")
    {
        QMessageBox::warning(this, tr("Add Hub"), tr("You should select a hub as the parent of the new hub !"));
        return;
    }
    bulkAdd->lblLabel->setText("Hub name (use * as placeholder):");
    resetBulkAddDialog();
    if (bulkAddDialog->exec() == QDialog::Accepted)
    {
        int finalValue = bulkAdd->spbFinalValue->value();
        int length = QString::number(finalValue).length();
        QString placeholder(bulkAdd->lneHostHubName->text());
        for (int i = bulkAdd->spbInitialValue->value(); i < finalValue; ++i)
            addHub(placeholder.replace("*", QString("%1").arg(i, length, 10, QChar('0'))));
    }

}

void MainWindow::on_actionRemoveHub_triggered()
{
    if (ui->trwHostArrays->selectedItems()[0] == ui->trwHostArrays->topLevelItem(0))
    {
        QMessageBox::warning(this, tr("Remove Hub"), tr("You can not remove the main hub !"));
        return;
    }
    if (ui->trwHostArrays->selectedItems()[0]->data(0, Qt::UserRole).toString() != "hub")
    {
        QMessageBox::warning(this, tr("Add Hub"), tr("You should select a hub to be removed !"));
        return;
    }
    if (QMessageBox::question(this, tr("Remove Hub"),
                              tr("Are you sure you want to remove hub %1 and all its children ?").arg(ui->trwHostArrays->selectedItems()[0]->text(0)), QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes)
        delete ui->trwHostArrays->selectedItems()[0];
}

void MainWindow::on_actionAddHost_triggered()
{
    if (ui->trwHostArrays->selectedItems()[0]->data(0, Qt::UserRole).toString() != "hub")
    {
        QMessageBox::warning(this, tr("Add Host"), tr("You should select a hub as the parent of the new host !"));
        return;
    }
    bool ok;
    QString hostName = QInputDialog::getText(this, tr("Add Host"), tr("Host name:"), QLineEdit::Normal, "", &ok);
    if (ok && !hostName.isEmpty())
        addHost(hostName);
}

void MainWindow::on_actionBulkAddHost_triggered()
{
    if (ui->trwHostArrays->selectedItems()[0]->data(0, Qt::UserRole).toString() != "hub")
    {
        QMessageBox::warning(this, tr("Add Host"), tr("You should select a hub as the parent of the new host !"));
        return;
    }
    bulkAdd->lblLabel->setText("Host name (use * as placeholder):");
    resetBulkAddDialog();
    if (bulkAddDialog->exec() == QDialog::Accepted)
    {
        int finalValue = bulkAdd->spbFinalValue->value();
        int length = QString::number(finalValue).length();
        QString placeholder(bulkAdd->lneHostHubName->text());
        for (int i = bulkAdd->spbInitialValue->value(); i < finalValue; ++i)
            addHost(placeholder.replace("*", QString("%1").arg(i, length, 10, QChar('0'))));
    }
}

void MainWindow::on_actionRemoveHost_triggered()
{
    if (ui->trwHostArrays->selectedItems()[0]->data(0, Qt::UserRole).toString() != "host")
    {
        QMessageBox::warning(this, tr("Add Hub"), tr("You should select a host to be removed !"));
        return;
    }
    if (QMessageBox::question(this, tr("Remove Host"),
                              tr("Are you sure you want to remove host %1 ?").arg(ui->trwHostArrays->selectedItems()[0]->text(0)), QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes)
        delete ui->trwHostArrays->selectedItems()[0];
}

void MainWindow::on_actionScanNetwork_triggered()
{
    if (ui->trwHostArrays->selectedItems()[0]->data(0, Qt::UserRole).toString() != "hub")
    {
        QMessageBox::warning(this, tr("Add Host"), tr("You should select a hub as the parent of found hosts !"));
        return;
    }
    bool ok;
    QString ipRange = QInputDialog::getText(this, tr("Scan Network"), tr("IP range to be scanned (use * as wildcard):"), QLineEdit::Normal, "", &ok);
    if (ok)
    {
        if (ipRange.startsWith('*'))
        {
            QMessageBox::warning(this, tr("Scan Network"), tr("You should specify at least the first octect, eg. 10.*.*.* !"));
            return;
        }
        if (QHostAddress(QString(ipRange).replace("*", "0")).protocol() != QAbstractSocket::IPv4Protocol)
        {
            QMessageBox::warning(this, tr("Scan Network"), tr("Please enter a valid IP network range !"));
            return;
        }
        QStringList octect = ipRange.split('.');
        _iteratingOctect = -1;
        for (int i = 0; i < 4; ++i)
        {
            _octect[i] = (octect.at(i) != "*") ? octect.at(i).toInt():0;
            _octectFixed[i] = octect.at(i) != "*";
            _iteratingOctect = (octect.at(i) == "*") ? i:_iteratingOctect;
        }
        _lastIteratingOctect = _iteratingOctect;
        _detectedHosts.clear();
        ui->lswFoundHosts->clear();
        ui->lblScanningHost->setText(QString("Scanning host: %1").arg(QString("%1.%2.%3.%4").arg(_octect[0]).arg(_octect[1]).arg(_octect[2]).arg(_octect[3])));
        _scanCanceled = false;
        _tcpSocket->connectToHost(QString("%1.%2.%3.%4").arg(_octect[0]).arg(_octect[1]).arg(_octect[2]).arg(_octect[3]), 22);
        QTimer::singleShot(settings->spbNetworkScanTimeout->value(), this, SLOT(tcpSocketTimeout()));
        ui->pgbNetworkScan->setMaximum(pow(256, ipRange.count('*')));
        ui->pgbNetworkScan->setValue(0);
        ui->dckNetworkScan->setVisible(true);
    }
}

void MainWindow::on_actionConnect_triggered()
{
}

void MainWindow::on_actionDisconnect_triggered()
{
}

void MainWindow::on_actionSpokSettings_triggered()
{
    if (settingsDialog->exec() == QDialog::Accepted)
        writeSettings();
    else
        readSettings();
}

void MainWindow::on_psbCancelNetworkScan_clicked()
{
    _scanCanceled = true;
}

void MainWindow::socketStateChanged(QAbstractSocket::SocketState state)
{
    if (state == QAbstractSocket::ConnectedState || state == QAbstractSocket::UnconnectedState)
    {
        if (_octect[_iteratingOctect] < 255)
        {
            ui->pgbNetworkScan->setValue(ui->pgbNetworkScan->value()+1);
            if (state == QAbstractSocket::ConnectedState)
                ui->lswFoundHosts->addItem(QString("%1.%2.%3.%4").arg(_octect[0]).arg(_octect[1]).arg(_octect[2]).arg(_octect[3]));

            _octect[_iteratingOctect]++;
            _tcpSocket->abort();

            if (!_scanCanceled)
            {
                ui->lblScanningHost->setText(QString("Scanning host: %1").arg(QString("%1.%2.%3.%4").arg(_octect[0]).arg(_octect[1]).arg(_octect[2]).arg(_octect[3])));
                _tcpSocket->connectToHost(QString("%1.%2.%3.%4").arg(_octect[0]).arg(_octect[1]).arg(_octect[2]).arg(_octect[3]), 22);
                QTimer::singleShot(settings->spbNetworkScanTimeout->value(), this, SLOT(tcpSocketTimeout()));
            }
            else
                confirmBulkAdd();
        }
        else
        {
            _octect[_iteratingOctect] = 0;
            _iteratingOctect--;
            while (_octectFixed[_iteratingOctect] || _octect[_iteratingOctect] == 255)
                _iteratingOctect--;
            if(_iteratingOctect >= 0)
            {
                _octect[_iteratingOctect]++;
                _iteratingOctect++;
                while (_iteratingOctect < _lastIteratingOctect)
                {
                    if (!_octectFixed[_iteratingOctect])
                        _octect[_iteratingOctect] = 0;
                    _iteratingOctect++;
                }
                _tcpSocket->abort();
                if (!_scanCanceled)
                {
                    ui->lblScanningHost->setText(QString("Scanning host: %1").arg(QString("%1.%2.%3.%4").arg(_octect[0]).arg(_octect[1]).arg(_octect[2]).arg(_octect[3])));
                    _tcpSocket->connectToHost(QString("%1.%2.%3.%4").arg(_octect[0]).arg(_octect[1]).arg(_octect[2]).arg(_octect[3]), 22);
                    QTimer::singleShot(settings->spbNetworkScanTimeout->value(), this, SLOT(tcpSocketTimeout()));
                }
                else
                    confirmBulkAdd();
            }
            else
                confirmBulkAdd();
        }
    }
}

void MainWindow::rdbKeyPairClicked()
{
    settings->lblUsername->setEnabled(false);
    settings->lblPassword->setEnabled(false);
    settings->lneUsername->setEnabled(false);
    settings->lnePassword->setEnabled(false);
    settings->lneUsername->clear();
    settings->lnePassword->clear();

    settings->lblKeyFilesPrefix->setEnabled(true);
    settings->lblPassphrase->setEnabled(true);
    settings->lneKeyFilesPrefix->setEnabled(true);
    settings->lnePassphrase->setEnabled(true);
}

void MainWindow::rdbUsernamePasswordClicked()
{
    settings->lblKeyFilesPrefix->setEnabled(false);
    settings->lblPassphrase->setEnabled(false);
    settings->lneKeyFilesPrefix->setEnabled(false);
    settings->lnePassphrase->setEnabled(false);
    settings->lneKeyFilesPrefix->clear();
    settings->lnePassphrase->clear();

    settings->lblUsername->setEnabled(true);
    settings->lblPassword->setEnabled(true);
    settings->lneUsername->setEnabled(true);
    settings->lnePassword->setEnabled(true);
}

void MainWindow::tcpSocketTimeout()
{
    if (_tcpSocket->state() == QAbstractSocket::ConnectingState)
        _tcpSocket->abort();
}

void MainWindow::addHub(const QString &hubName)
{
    QTreeWidgetItem *item = new QTreeWidgetItem(ui->trwHostArrays->selectedItems()[0], QStringList() << hubName);
    item->setData(0, Qt::UserRole, "hub");
    item->setIcon(0, QIcon(":/resources/images/hub.png"));
    ui->trwHostArrays->expandItem(ui->trwHostArrays->selectedItems()[0]);
}

void MainWindow::addHost(const QString &hostName)
{
    QTreeWidgetItem *item = new QTreeWidgetItem(ui->trwHostArrays->selectedItems()[0], QStringList() << hostName);
    item->setData(0, Qt::UserRole, "host");
    item->setIcon(0, QIcon(":/resources/images/host.png"));
    ui->trwHostArrays->expandItem(ui->trwHostArrays->selectedItems()[0]);
}

void MainWindow::resetBulkAddDialog()
{
    bulkAdd->lneHostHubName->clear();
    bulkAdd->spbInitialValue->setValue(0);
    bulkAdd->spbFinalValue->setValue(9);
}

void MainWindow::confirmBulkAdd()
{
    ui->pgbNetworkScan->setValue(ui->pgbNetworkScan->maximum());
    if (ui->lswFoundHosts->count() > 0)
    {
        if (QMessageBox::question(this, tr("Network Scan"),
                                  tr("Would you like to add found hosts as child of %1 hub ?").arg(ui->trwHostArrays->selectedItems()[0]->text(0)), QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes)
        {
            int size = ui->lswFoundHosts->count();
            for(int i = 0; i < size; ++i)
                addHost(ui->lswFoundHosts->item(i)->text());
        }
    }
    ui->dckNetworkScan->setVisible(false);
}
